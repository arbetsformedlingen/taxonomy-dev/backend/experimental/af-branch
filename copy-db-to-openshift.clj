#!/usr/bin/env bb
(require '[babashka.process :refer [shell process exec]])

;; You need to create a Persistent volume claim in the project you are working in
;; to get this to work




(def db-name  "taxonomy-AF-v2001.nippy")

(shell "oc" "apply" "-f" "resources/oc/busybox-helper.yaml")
(println "Sleeping 30 s")
(shell "sleep" "30")
(println "copying database " db-name)
(shell "oc" "cp" (str "resources/" db-name) "busybox-helper:/mnt/datahike")
(println "change permissions in folder /mnt/datahike/" db-name)
(shell "oc" "exec" "pod/busybox-helper" "--" "chmod" "666" (str "/mnt/datahike/" db-name))
(shell "oc" "delete" "pod" "busybox-helper")
