^{:nextjournal.clerk/visibility #{:hide}}
(ns jobtech-taxonomy.delta
  (:require [datahike.api :as d]
            [datomic.client.api :as dc]
            [taoensso.nippy :as nippy]
            [clojure.string :as st]
            [clojure.set :as s]
            [wanderung.core :as w]
            [clj-http.client :as client]
            [mount.core :refer [defstate]]
            [jobtech-taxonomy-common.db-schema :as db-schema]
            [jobtech-taxonomy-common.relation :as r]
            [jobtech-taxonomy.af-branch :as afb]
            [nextjournal.clerk :as clerk]

            [dk.ative.docjure.spreadsheet :as dj]

            )
  (:gen-class))






(comment
  (clerk/serve! {:browse true})

  )

;; https://taxonomy-i1.api.jobtechdev.se/v1/taxonomy/main/concept/changes?after-version=1&to-version-inclusive=2001


(defn get-changes []
  (:body (client/get "https://taxonomy-i1.api.jobtechdev.se/v1/taxonomy/main/concept/changes?after-version=1&to-version-inclusive=2001"
                     {:socket-timeout 10000 :connection-timeout 10000 :accept :json :as :json})))



(defn deprecated-nil? [change]
  (and
   (= "deprecated" (:taxonomy/attribute change))
   (not (:taxonomy/new-value change))
   )
  )

(defn filter-deprecated-nil [change]
  (some deprecated-nil? (:taxonomy/concept-attribute-changes change)))

#_(remove filter-deprecated-nil data)

(defn change->row [{:taxonomy/keys [version event-type latest-version-of-concept]}]
  [version
   event-type
   (:taxonomy/type latest-version-of-concept)
   (:taxonomy/preferred-label latest-version-of-concept)
   (:taxonomy/id latest-version-of-concept)
   ]
  )




(clerk/table
 {:head ["Version" "change" "type" "label" "id"]
  :rows (map change->row (remove filter-deprecated-nil (get-changes)))
  })


(defn load-relation-changes []
  (clojure.edn/read-string (slurp "resources/relation-changes.edn") )
  )

(defn is-comment [relation-change]
  (= "COMMENTED" (:taxonomy/event-type relation-change)))

(defn relation-change->row [{:taxonomy/keys [event-type relation version]}]
  [version
   event-type
   (get-in relation [:taxonomy/relation-type])
   (get-in relation [:taxonomy/source :taxonomy/preferred-label])
   (get-in relation [:taxonomy/source :taxonomy/type])
   (get-in relation [:taxonomy/source :taxonomy/id])

   (get-in relation [:taxonomy/target :taxonomy/preferred-label])
   (get-in relation [:taxonomy/target :taxonomy/type])
   (get-in relation [:taxonomy/target :taxonomy/id])
   ]
  )


(clerk/table
 {:head ["Version" "change" "Relation Type" "Source Label" "Source type" "Source id" "Target Label" "Target type" "Target id"]
  :rows (map relation-change->row (remove is-comment (load-relation-changes)))
  })




(defn save-as-excell []
  (let [wb (dj/create-workbook "Concept Changes"
                               (concat [["Version" "change" "type" "label" "id"]]
                                       (map change->row (remove filter-deprecated-nil (get-changes)))
                                    )
                            "Relation Changes"
                            (concat [["Version" "change" "Relation Type" "Source Label" "Source type" "Source id" "Target Label" "Target type" "Target id"]]
                                    (map relation-change->row (remove is-comment (load-relation-changes)))
                                    )


                            )]
    (dj/save-workbook! "taxonomy-2001-changes.xlsx" wb)))
