(ns jobtech-taxonomy.deploy
  (:require
   [clojure.java.shell :as shell])
  (:gen-class))


;; does not work!

#_(defn copy-database-to-openshift [db-name]
  (println "Starting busybox")
  (println (shell/sh "oc" "apply" "-f" "resources/oc/busybox-helper.yaml"))
  (println "Sleeping 30 s")
  (println (shell/sh "sleep" "30"))
  (println "copying database " db-name)
  (println (shell/sh "oc" "cp" (str "resources/" db-name) "busybox-helper:/mnt/datahike"))
  (println "change permissions in folder /mnt/datahike/" db-name)
  (println (shell/sh "oc" "exec" "pod/busybox-helper" "--" "chmod" "666" (str "/mnt/datahike/" db-name)))
  (println (shell/sh "oc" "delete" "pod" "busybox-helper")))


;; (copy-database-to-openshift "taxonomy-AF-v2001.nippy")

;; TODO migrate to babashka
