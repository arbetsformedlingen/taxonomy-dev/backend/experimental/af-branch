(ns jobtech-taxonomy.af-branch
  (:require [datahike.api :as d]
            [datomic.client.api :as dc]
            [taoensso.nippy :as nippy]
            [clojure.string :as st]
            [clojure.set :as s]
            [wanderung.core :as w]
            [clj-http.client :as client]
            [mount.core :refer [defstate]]
            [jobtech-taxonomy-common.db-schema :as db-schema]
            [jobtech-taxonomy-common.relation :as r]
            [clojure.pprint :as pp]
            [clojure.edn :as edn]
            [malli.core :as m]
            )
  (:gen-class))

;; TODO put all transactions in edn files


(defn pretty-spit
  [file-name collection]
  (spit (java.io.File. file-name)
        (with-out-str (pp/write collection :dispatch pp/code-dispatch))))

(defn read-edn [filename]
  (edn/read-string (slurp (str "resources/2001-transactions/" filename)))
  )

(def relation-schema
  (m/schema [:sequential [:map
                          [:relation/id string?]
                           [:relation/concept-1 [:tuple keyword? string?]]
                           [:relation/type [:enum "broader"]]
                           [:relation/concept-2 [:tuple keyword? string?]]]]))

;; (m/validate relation-schema (read-edn "occupation-names-relation-16.edn"))


(defn greet
  "Callable entry point to the application."
  [data]
  (println (str "Hello, " (or (:name data) "World") "!")))

(defn -main
  "I don't do a whole lot ... yet."
  [& args]
  (greet {:name (first args)}))


(def datahike-cfg
  {:wanderung/type :datahike
   :store         {:backend :mem
                   :id      "taxonomy"}
   :index :datahike.index/persistent-set
   :keep-history? true})

(def af-v1-nippy-cfg
  {:wanderung/type :nippy
   :filename "resources/taxonomy-AF-v1.nippy"})

(def af-v2001-nippy-cfg
  {:wanderung/type :nippy
   :filename "resources/taxonomy-AF-v2001.nippy"})

(def datomic-prod-v2001
  {:wanderung/type :datomic
   :name "jobtech-taxonomy-AF-v2001"
   :server-type :ion
   :region      "eu-central-1"
   :system      "tax-prod-v4"
   :endpoint    "http://entry.tax-prod-v4.eu-central-1.datomic.net:8182/"
   :proxy-port  8182}
  )

(def datomic-prod-v16
  {:wanderung/type :datomic
   :name "jobtech-taxonomy-AF-v16"
   :server-type :ion
   :region      "eu-central-1"
   :system      "tax-prod-v4"
   :endpoint    "http://entry.tax-prod-v4.eu-central-1.datomic.net:8182/"
   :proxy-port  8182}
  )

(defn load-database []
  (w/migrate af-v1-nippy-cfg datahike-cfg))

(defn save-database []
  (w/migrate datahike-cfg af-v2001-nippy-cfg))



(defn load-v2001-inmem-database []
  (w/migrate af-v2001-nippy-cfg datahike-cfg))

(defn create-conn []
  (do
    (load-database)
    (d/connect datahike-cfg)))

(defstate  ^{:on-reload :noop} conn :start (create-conn))

;; ????? (mount.core/start) ???

(defn fetch-ssyk-level-4 [db id]
  (d/q
   '[:find ?c
     :where
     [?c :concept/id ?id]
     :in $ ?id
     ]
   db id))

(def grahpql-url "https://taxonomy.api.jobtechdev.se/v1/taxonomy/graphql?query=")

(defn grahpql-query [version]
  (format "query OccupationNames {
  occupation_name_v16: concepts(type: \"occupation-name\", version: %s, include_deprecated: false) {
    id
    preferred_label
    type
    alternative_labels
    hidden_labels
    definition
    broader {
      id
    }
  }
    occupation_name_v1: concepts(type: \"occupation-name\", version: 1) {
    id
  }
}" version)
  )


(defn get-occupation-names [version]
  (:body (client/get (str grahpql-url (grahpql-query version)) {:accept :json :as :json}))
  )

(def test-oc
  '{:id "86sy_hBf_exW",
    :preferred_label "Ämneslärare, gymnasieskolan",
    :type "occupation-name",
    :alternative_labels ["Lärare i gymnasieskolan"],
    :hidden_labels [],
    :definition "Ämneslärare, gymnasieskolan",
    :deprecated false,
    :broader [{:id "4KhP_FxL_uZ5"}]}
  )

(defn occupation-name->tx [occupation-name]
  (dissoc (clojure.set/rename-keys occupation-name {:db/id :concept/id
                                                      :id :concept/id
                                                      :type :concept/type
                                                      :definition :concept/definition
                                                      :preferred_label :concept/preferred-label
                                                      :alternative_labels :concept/alternative-labels
                                                      :hidden_labels :concept/hidden-labels
                                                      }) :broader))

(defn occupation-name-relation->tx [occupation-name]
  (map (fn [b-id]
         (r/edge-tx (:id occupation-name) "broader" (:id b-id)))
       (:broader occupation-name)))


;; gör en extra transaction innna relationerna!

(defn parse-occupation-names-response [resp]
  (let [occupation-name-v1 (set (map :id (get-in resp [:data :occupation_name_v1])))
        occupation-name-v16 (remove #(contains? occupation-name-v1 (:id %))
                                    (get-in resp [:data :occupation_name_v16]))
        ]
    occupation-name-v16))

(def unemployment-types-gql
  "query SKAT {
  concepts(type: \"unemployment-type\", version: 19) {
    id
    preferred_label
    alternative_labels
    hidden_labels
    unemployment_type_code
    definition
    type
  }
}"
  )


(defn get-unemployment-types []
  (:concepts (:data (:body (client/get (str grahpql-url unemployment-types-gql) {:accept :json :as :json}))))
  )

(defn parse-unemployment-types [response]
  (map #(s/rename-keys % {:id :concept/id
                          :preferred_label :concept/preferred-label
                          :alternative_labels :concept/alternative-labels
                          :hidden_labels :concept/hidden-labels
                          :unemployment_type_code :concept.external-standard/unemployment-type-code
                          :definition :concept/definition
                          :type :concept/type
                          })  response))


(def unemployment-fund-gql
  "query UNEMPLOYMENY_FUND {
  concepts(type: \"unemployment-fund\", version: 9, include_deprecated: true) {
    id
    preferred_label
    alternative_labels
    hidden_labels
    unemployment_fund_code
    definition
    type
    deprecated
  }
}"
  )

(defn parse-unemployment-fund [response]
  (map #(s/rename-keys % {:id :concept/id
                          :preferred_label :concept/preferred-label
                          :alternative_labels :concept/alternative-labels
                          :hidden_labels :concept/hidden-labels
                          :unemployment_fund_code :concept.external-standard/unemployment-fund-code-2017
                          :definition :concept/definition
                          :type :concept/type
                          :deprecated :concept/deprecated
                          })  response))


(defn get-unemployment-fund []
  (:concepts (:data (:body (client/get (str grahpql-url unemployment-fund-gql) {:accept :json :as :json}))))
  )


;; TODO skapa en version 6 för occuaption collection och 2001 som är baserad på 16

(def occupation-collection-gql
  "query OCCUPATION_COLLECTION {
  concepts(type: \"occupation-collection\", version: %s, include_deprecated: true) {
    id
    preferred_label
    alternative_labels
    hidden_labels
    definition
    type

    related (type: \"occupation-name\"){
      id
    }
  }
}")

;; TODO Vissa  collections idn är nya så de kan inte refereraas som tmp id
;; separera relations transaktioner och rena concept transaktioner

(defn parse-occupation-collection [response]
  (mapcat
   (fn [c]
     (concat
      [(dissoc
       (s/rename-keys
        c
        {:id :concept/id
         :preferred_label :concept/preferred-label
         :alternative_labels :concept/alternative-labels
         :hidden_labels :concept/hidden-labels
         :definition :concept/definition
         :type :concept/type}) :related)]
       (map
        (fn [related-id]
          (r/edge-tx (:id c)
                     "related"
                     (:id related-id)
                     #_{::r/concept-1-tempid
                      (:id c)
                      ::r/concept-2-tempid
                      (:id related-id)}))
               (:related c))))
   response))

(defn parse-occupation-collection-to-concepts-and-relations [parsed-oc]
  {:occupation-collection-concepts (filter :concept/id  parsed-oc)
   :occupation-collection-relations (filter :relation/id  parsed-oc)
   })


(defn get-occupation-collection [version]
  (:concepts (:data (:body (client/get (str grahpql-url (format occupation-collection-gql version)) {:accept :json :as :json}))))
  )

(def graphql-languages
  "query LANGUAGES {
  concepts(type: \"language\", include_deprecated: true, version: 19) {
    id
    preferred_label
    alternative_labels
    hidden_labels
    definition
    type
    deprecated
    iso_639_3_alpha_3_2007
    iso_639_3_alpha_2_2007
    iso_639_1_2002
    iso_639_3_2007
    iso_639_2_1998
    broader {
      id
      preferred_label
      alternative_labels
      hidden_labels
      type
      definition
    }
  }
}")

(defn get-languages []
  (:concepts (:data (:body (client/get (str grahpql-url graphql-languages) {:accept :json :as :json}))))
  )


(def l-example
  {:iso_639_3_alpha_3_2007 "AAR",
   :definition "Afar",
   :preferred_label "Afar/Afariska",
   :iso_639_3_alpha_2_2007 "AA",
   :iso_639_3_2007 nil,
   :iso_639_2_1998 "AAR",
   :type "language",
   :deprecated false,
   :iso_639_1_2002 "AA",
   :id "5vWL_41L_udS",
   :broader
   [{:id "ngh6_UjL_9bg",
     :preferred_label "Tolkspråk",
     :type "language-collection",
     :definition
     "Språk kopplade till språksamlingen är tillgängliga för tolkbeställning."}]}
  )

(defn parse-langauge [{:keys [id
                              type
                              definition
                              preferred_label
                              deprecated
                              iso_639_3_alpha_2_2007
                              iso_639_3_alpha_3_2007
                              iso_639_3_2007
                              iso_639_2_1998
                              iso_639_1_2002
                              broader
                              alternative_labels
                              hidden_labels
                              ]}]


  (remove nil?  [   (cond->
                        {:concept/id id
                         :concept/type type
                         :concept/definition definition
                         :concept/deprecated deprecated
                         :concept/preferred-label preferred_label
                         :concept/alternative-labels alternative_labels
                         :concept/hidden-labels hidden_labels
                         }
                      iso_639_1_2002 (assoc :concept.external-standard/iso-639-1-2002 iso_639_1_2002)
                      iso_639_2_1998  (assoc :concept.external-standard/iso-639-2-1998 iso_639_2_1998)
                      iso_639_3_2007  (assoc :concept.external-standard/iso-639-3-2007 iso_639_3_2007)
                      iso_639_3_alpha_2_2007 (assoc :concept.external-standard/iso-639-3-alpha-2-2007 iso_639_3_alpha_2_2007)
                      iso_639_3_alpha_3_2007 (assoc :concept.external-standard/iso-639-3-alpha-3-2007 iso_639_3_alpha_3_2007)
                      )

                 (if (not (empty? broader))

                   (r/edge-tx id "broader" (:id (first broader)))

                   )]))

(defn parse-langauges [response]
  (mapcat parse-langauge response)
  )


;; TODO FIX keywords
;; TODO fix tolkspråk koppling

;; https://taxonomy-i1.api.jobtechdev.se/v1/taxonomy/main/concepts?related-ids=ngh6_UjL_9bg&relation=narrower

"AIS-klienten hämtar in unemployment-fund  version 9 och occcupation-collection version 6 vid uppstart, så de går inte att läsa in längre

Dessutom hämtar den latest för unemployment-type, så de som tillkommit saknas.
AIS-F backend hämtar latest för unemployment-type och unemployment-fund, så samma problem där"

;; Change version name to 2001



;; Lägg till occupation names från version 6 ocskå



(defn apply-version-2001 [transact cn]
  (let [#_#_cn (create-conn)
        occupation-names-v16 (parse-occupation-names-response (get-occupation-names 16))
        occupation-names-v16-tx (map occupation-name->tx occupation-names-v16)
        occupation-names-relation-v16-tx (mapcat occupation-name-relation->tx occupation-names-v16)


        occupation-names-v6 (parse-occupation-names-response (get-occupation-names 6))
        occupation-names-v6-tx (map occupation-name->tx occupation-names-v6)
        occupation-names-relation-v6-tx (mapcat occupation-name-relation->tx occupation-names-v6)



        {:keys [occupation-collection-concepts
                occupation-collection-relations
                ]} (parse-occupation-collection-to-concepts-and-relations (parse-occupation-collection (get-occupation-collection 6)))

        ]


    (println "latest schema")
    (transact cn {:tx-data db-schema/schema})


    ;; (println "occupation-names v 6    concepts")
    ;; (transact cn {:tx-data occupation-names-v6-tx})
    ;; (println "occupation-names v 6   relations" )
    ;; (transact cn {:tx-data occupation-names-relation-v6-tx})



    ;; (println "occupation-collection 6    concepts")
    ;; (transact cn {:tx-data occupation-collection-concepts})

    ;; (println "occupation-collection 6    relations")
    ;; (transact cn {:tx-data occupation-collection-relations})
    ;; (transact cn {:tx-data [{:taxonomy-version/id 6
    ;;                            :taxonomy-version/tx "datomic.tx"}]})


    ;; (println "unemployment-fund 9")
    ;; (transact cn {:tx-data (parse-unemployment-fund (get-unemployment-fund))})
    ;; (transact cn {:tx-data [{:taxonomy-version/id 9
    ;;                            :taxonomy-version/tx "datomic.tx"}]})

    ;; (println "language-collection")
    ;; (transact cn {:tx-data [{:concept/id "ngh6_UjL_9bg",
    ;;                            :concept/preferred-label "Tolkspråk",
    ;;                            :concept/type "language-collection",
    ;;                            :concept/definition
    ;;                            "Språk kopplade till språksamlingen är tillgängliga för tolkbeställning."}]})

    ;; (println "languages 15")
    ;; (transact cn {:tx-data (filter :concept/id (parse-langauges (get-languages)))})
    ;; (transact cn {:tx-data (filter :relation/id (parse-langauges (get-languages)))})
    ;; (transact cn {:tx-data [{:taxonomy-version/id 15
    ;;                            :taxonomy-version/tx "datomic.tx"}]})

    (println "occupation-names v 16")
    (transact cn {:tx-data occupation-names-v16-tx})
    (transact cn {:tx-data occupation-names-relation-v16-tx})

;;    (transact cn {:tx-data (parse-unemployment-types (get-unemployment-types))})

;;    (println "occupation-collection v 16")
    (transact cn {:tx-data (filter :concept/id (parse-occupation-collection (get-occupation-collection 16)))})
    (transact cn {:tx-data (filter :relation/id (parse-occupation-collection (get-occupation-collection 16)))})

    ;; Storbritanien är inte med i EU

;; TODO ta bort EU relationen, verkar inte fungera

    ;; (println "UK out of EU")
    ;; (transact cn {:tx-data [[:db/retractEntity [:relation/id "7wHq_Mri_wCt:broader:HstV_nCB_W2i"]]]})

    ;; (transact cn {:tx-data [(r/edge-tx "7wHq_Mri_wCt"  "broader" "TWjU_Uga_11a")]})

    ;; (println "commiting v 16")
    ;; (transact cn {:tx-data [{:taxonomy-version/id 16
    ;;                            :taxonomy-version/tx "datomic.tx"}]})

    ;; (println "commiting v19")
    ;; (transact cn {:tx-data [{:taxonomy-version/id 19
    ;;                          :taxonomy-version/tx "datomic.tx"}]})

    (println "commiting v2001")
    (transact cn {:tx-data [{:taxonomy-version/id 2001
                               :taxonomy-version/tx "datomic.tx"}]})
    #_(save-database)))


#_(defn apply-version-2001-step-2)

(defn migrate-version-2001-to-prod []
  (w/migrate af-v2001-nippy-cfg datomic-prod-v2001))


(defn migrate-version-1-to-prod []
  (w/migrate af-v1-nippy-cfg datomic-prod-v2001))

;;     "Time conflict: Fri Mar 05 13:32:59 UTC 2021 is older than database basis"

(comment

  ;;   (def client (dc/client datomic-prod-v16))
  ;;   (dc/delete-database client {:db-name "jobtech-taxonomy-AF-v2001"})
  ;;   (dc/create-database client {:db-name "jobtech-taxonomy-AF-v2001"})

  ;;   (migrate-version-1-to-prod)

  )

(defn delete-database-v2001! []
  (let [ client (dc/client datomic-prod-v16)]
    (dc/delete-database client {:db-name "jobtech-taxonomy-AF-v2001"})
    ))

(defn create-database-v2001! []
  (let [ client (dc/client datomic-prod-v16)]
    (dc/create-database client {:db-name "jobtech-taxonomy-AF-v2001"})
    ))

(defn create-v2001-in-datomic-prod []
  ;; (migrate-version-1-to-prod)

  (let [client (dc/client datomic-prod-v2001)
        conn (dc/connect client {:db-name "jobtech-taxonomy-AF-v2001"})
        ]
    (apply-version-2001 dc/transact conn)))

;; save database
;; (w/migrate datomic-prod-v2001 af-v2001-nippy-cfg)
