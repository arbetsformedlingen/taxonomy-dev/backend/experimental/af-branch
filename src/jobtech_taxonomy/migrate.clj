(ns jobtech-taxonomy.migrate
  (:require [datahike.api :as d]
            [datomic.client.api :as dc]
            [taoensso.nippy :as nippy]
            [clojure.string :as st]
            [clojure.set :as s]
            [wanderung.core :as w]
            [clj-http.client :as client]
            [mount.core :refer [defstate]]
            [jobtech-taxonomy-common.db-schema :as db-schema]
            )
  (:gen-class))


(def datomic-test-cfg
  {:name "jobtech-taxonomy-dev-2022-09-09-13-50-37"
   :server-type :ion
   :region      "eu-central-1"
   :system      "tax-test-v4"
   :endpoint   "https://hcvc9p1nvi.execute-api.eu-central-1.amazonaws.com/"
   }
  )

(def  client-test
  (dc/client datomic-test-cfg))




(def v19-nippy-cfg
  {:wanderung/type :nippy
   :filename "resources/taxonomy-v19.nippy"})

(def v20-nippy-cfg
  {:wanderung/type :nippy
   :filename "resources/taxonomy-v20.nippy"})


(def datomic-test-v19
  {:wanderung/type :datomic
   :name "jobtech-taxonomy-test-v19"
   :server-type :ion
   :region      "eu-central-1"
   :system      "tax-test-v4"
   :endpoint   "https://hcvc9p1nvi.execute-api.eu-central-1.amazonaws.com/"
  }
  )

(def datomic-test-in-prod-v20
  {:wanderung/type :datomic
   :name "jobtech-taxonomy-test-in-prod-v20"
   :server-type :ion
   :region      "eu-central-1"
   :system      "tax-prod-v4"
   :endpoint    "http://entry.tax-prod-v4.eu-central-1.datomic.net:8182/"
   :proxy-port  8182}
  )

(def datomic-prod-v16
  {:wanderung/type :datomic
   :name "jobtech-taxonomy-AF-v16"
   :server-type :ion
   :region      "eu-central-1"
   :system      "tax-prod-v4"
   :endpoint    "http://entry.tax-prod-v4.eu-central-1.datomic.net:8182/"
   :proxy-port  8182}
  )

(def  client-prod
  (dc/client datomic-prod-v16))

#_(def datomic-prod-v2001
  {:wanderung/type :datomic
   :name "jobtech-taxonomy-AF-v2001"
   :server-type :ion
   :region      "eu-central-1"
   :system      "tax-prod-v4"
   :endpoint    "http://entry.tax-prod-v4.eu-central-1.datomic.net:8182/"
   :proxy-port  8182}
  )


(defn migrate-to-test-database []
  (w/migrate v20-nippy-cfg datomic-test-v19))

(comment


  ;; (dc/delete-database client {:db-name "jobtech-taxonomy-test-v19"})
 ;;  (dc/create-database client {:db-name "jobtech-taxonomy-test-v19"})

  ;; (dc/create-database client-test {:db-name "jobtech-taxonomy-test-v19"})


  ;; (w/migrate v20-nippy-cfg datomic-test-in-prod-v20)

  )
